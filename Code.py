# # Correlation in Python (Pearson Correlation)

# Correlation refers to some statistical relationships involving dependence between two data sets. Simple examples of dependent phenomena include the correlation between the physical appearance of parents and their offspring, and the correlation between the price for a product and its supplied quantity.

# ## Methods for correlation analyses:

# 1. Parametric Correlation : It measures a linear dependence between two variables (x and y) is known as a parametric correlation test because it depends on the distribution of the data.
# 2. Non-Parametric Correlation: Kendall(tau) and Spearman(rho), which are rank-based correlation coefficients, are known as non-parametric correlation.
# Note: The most commonly used method is the Parametric correlation method.

# ### Example

# We take example of the iris data set available in seaborn python library. In it we try to establish the correlation between the length and the width of the sepals and petals of three species of iris flower. Based on the correlation found, a strong model could be created which easily distinguishes one species from another.

# In[3]:


import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


# In[4]:


df = pd.read_csv('/home/aarush100616/Downloads/Projects/Iris Flower Classification/Iris.csv')


# In[5]:


#without regression
sns.pairplot(df, kind="scatter")
plt.show()


# ## Pearson Correlation

# Measures the strength of correlation between two features:
# 1. Correlation coefficient
# 2. P-value

# ### Correlation Coefficient:
# 1. Close to +1: Large positive relationship
# 2. Close to -1: Large negative relationship
# 3. Close to 0:  No relationship
#     
# ### P-value:
# 1. P-value<0.001: Strong certainty in the result
# 2. P-value<0.05:  Moderate certainty in the result
# 3. P-value<0.1:   Weak certainty in the result
# 4. P-value>0.1:   No certainty in the result

# ### To compute Pearson correlation in Python – pearsonr() function can be used.

# ### Implementation

# In[6]:


# Importing libraries
import pandas as pd
from scipy.stats import pearsonr


# In[7]:


df = pd.read_csv("/home/aarush100616/Downloads/Projects/Correlation/Auto.csv")


# In[8]:


df


# In[9]:


list1 = df['weight']
list2 = df['mpg']


# In[10]:


list1


# In[11]:


list2


# In[12]:


# Apply the pearsonr()
corr, _ = pearsonr(list1, list2)
print('Pearsons correlation: %.3f' % corr)
